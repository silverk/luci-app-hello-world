## Installing feed

- Add feed to OpenWRT buildroot `feeds.conf`. [Read in addition how to work with feeds](https://openwrt.org/docs/guide-developer/build-system/use-buildsystem).

Copy feeds defaults:

```
$ cp feeds.conf.default feeds.conf
```

For feed from git repo add to `feeds.conf`:

```
src-git luci_hello_world https://gitlab.com/silverk/luci-app-hello-world.git
```

For feed from filesystem add to `feeds.conf`:

```
src-link luci_hello_world /[path to files]/luci-app-hello-world
```

- Update feed and install app:

```
$ ./scripts/feeds update luci_hello_world
$ ./scripts/feeds install luci-app-hello-world
```

Execute menuconfig:

```
$ make menuconfig
```

and select package from the menu:

```
LuCI --->
    3. Applications --->
        luci-app-hello-world................................ LuCI hello world app (NEW)
```

## Build single package

Execute:

```
$ make package/luci-app-hello-world/compile V=s
```

package is built into directory `bin/[ARCH]/packages/luci_hello_world/`

## Install single package

[Read in addition how to use opkg package manager from command line](https://openwrt.org/docs/guide-user/additional-software/opkg).

Download package to OpenWRT file system `/tmp` directory:

```
# curl [link to ipk file] -o luci-app-hello-world_0.0.2-1_all.ipk
```

Install package

```
# opkg install luci-app-hello-world_0.0.2-1_all.ipk
```
